package com.mycompany.app;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;


import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;



/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	try {
			FileInputStream file = new FileInputStream(new File("input.xls"));
            
			//Get the workbook instance for XLS file
			try {
				
				
				
				
				
				HSSFWorkbook workbook = new HSSFWorkbook(file);
				//Get first sheet from the workbook
				HSSFSheet sheet = workbook.getSheetAt(0);
				sheet.autoSizeColumn(0);
				
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();

		}
    }
}
